import { Redis } from "ioredis";

const redis = new Redis({
    host: 'redis-service',
    port: 6379
})

redis.on('connect', async () => {
    console.log('get result')
    const result = await redis.get('test')

    console.log('result', result)

    redis.disconnect()
})


